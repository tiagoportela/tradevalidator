package com.tiagoportela.models;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.tiagoportela.contraints.Iso4217CurrencyPair;
import com.tiagoportela.contraints.SupportedCounterparty;

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
	@JsonSubTypes.Type(value = SpotTrade.class, name = "Spot"),
	@JsonSubTypes.Type(value = ForwardTrade.class, name = "Forward"),
	@JsonSubTypes.Type(value = VanillaTrade.class, name = "VanillaOption"),
})
public abstract class GeneralTrade {
	
	@SupportedCounterparty
	@NotEmpty
	private String customer;

	@Iso4217CurrencyPair
	@JsonProperty("ccyPair")
	@NotEmpty
	private String currencyPair;

	@NotEmpty
	private String type;

	@NotEmpty
	private String direction;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate tradeDate;

	@NotEmpty
	private String legalEntity;
	
	@NotEmpty
	private String trader;
	
	private BigDecimal amount1;
	private BigDecimal amount2;
	private Double rate;

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getCurrencyPair() {
		return currencyPair;
	}

	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public LocalDate getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(LocalDate tradeDate) {
		this.tradeDate = tradeDate;
	}

	public BigDecimal getAmount1() {
		return amount1;
	}

	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}

	public BigDecimal getAmount2() {
		return amount2;
	}

	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public String getLegalEntity() {
		return legalEntity;
	}

	public void setLegalEntity(String legalEntity) {
		this.legalEntity = legalEntity;
	}

	public String getTrader() {
		return trader;
	}

	public void setTrader(String trader) {
		this.trader = trader;
	}
}
