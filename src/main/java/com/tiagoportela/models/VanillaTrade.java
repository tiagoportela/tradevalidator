package com.tiagoportela.models;

import java.time.LocalDate;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tiagoportela.contraints.AmericanOptionExerciseDateValidation;
import com.tiagoportela.contraints.ExpiryAndPremiumDateBeforeDeliveryDate;
import com.tiagoportela.contraints.Iso4217Currency;
import com.tiagoportela.contraints.ValidOptionStyle;

@AmericanOptionExerciseDateValidation(optionStyleField = "style", exerciseDateField = "exerciseDate", tradeDateField = "tradeDate", expiryDateField="expiryDate")
@ExpiryAndPremiumDateBeforeDeliveryDate(expiryDateField = "expiryDate", premiumDateField = "premiumDate", deliveryDateField = "deliveryDate")
public class VanillaTrade extends GeneralTrade {
	
	@NotEmpty
	@ValidOptionStyle
	private String style;
	
	@NotEmpty
	private String strategy;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate deliveryDate;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate expiryDate;
	
	@NotEmpty
	@JsonProperty("payCcy")
	@Iso4217Currency
	private String payCurrency;
	
	@NotEmpty
	@JsonProperty("premiumCcy")
	@Iso4217Currency
	private String premiumCurrency;
	
	@NotEmpty
	private String premiumType;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate premiumDate;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate exerciseDate;
	
	private double premium;
	
	@Override
	public String toString() {
		return String.format("{customer:%s,ccyPair:%s,type:%s,direction:%s,tradeDate:$s,amount1:%s,amount2:%s,rate:%s,legalEntity:%s,trader:%s,"
				+ "style:%s,strategy:%s,deliveryDate:%s,expiryDate:%s,payCcy:%s,premium:%s,premiumCyy:%s,premiumType:%s,premiumDate:%s,exerciseDate:%s}", 
				this.getCustomer(), this.getCurrencyPair(), this.getType(), this.getDirection(), this.getTradeDate(), 
				this.getAmount1().toString(), this.getAmount2().toString(), this.getRate().toString(), this.getLegalEntity(), this.getTrader(),
				this.style, this.strategy, this.deliveryDate, this.expiryDate, this.payCurrency, this.premium, this.premiumCurrency, 
				this.premiumType, this.premiumDate, this.exerciseDate);
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getStrategy() {
		return strategy;
	}

	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}

	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public LocalDate getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getPayCurrency() {
		return payCurrency;
	}

	public void setPayCurrency(String payCurrency) {
		this.payCurrency = payCurrency;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

	public String getPremiumCurrency() {
		return premiumCurrency;
	}

	public void setPremiumCurrency(String premiumCurrency) {
		this.premiumCurrency = premiumCurrency;
	}

	public String getPremiumType() {
		return premiumType;
	}

	public void setPremiumType(String premiumType) {
		this.premiumType = premiumType;
	}

	public LocalDate getPremiumDate() {
		return premiumDate;
	}

	public void setPremiumDate(LocalDate premiumDate) {
		this.premiumDate = premiumDate;
	}

	public LocalDate getExerciseDate() {
		return exerciseDate;
	}

	public void setExerciseDate(LocalDate exerciseDate) {
		this.exerciseDate = exerciseDate;
	}
}
