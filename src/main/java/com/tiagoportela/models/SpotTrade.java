package com.tiagoportela.models;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tiagoportela.contraints.TradeDateBeforeValueDate;
import com.tiagoportela.contraints.ValidCurrencyDate;

@TradeDateBeforeValueDate(tradeDateField = "tradeDate", valueDateField = "valueDate")
public class SpotTrade extends GeneralTrade {
	
	@ValidCurrencyDate
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate valueDate;
	
	@Override
	public String toString() {
		return String.format("{customer:%s,ccyPair:%s,type:%s,direction:%s,tradeDate:$s,amount1:%s,amount2:%s,rate:%s,valueDate:%s,legalEntity:%s,trader:%s}", 
				this.getCustomer(), this.getCurrencyPair(), this.getType(), this.getDirection(), this.getTradeDate(), 
				this.getAmount1().toString(), this.getAmount2().toString(), this.getRate().toString(), valueDate, this.getLegalEntity(), this.getTrader());
	}
	
	public LocalDate getValueDate() {
		return valueDate;
	}

	public void setValueDate(LocalDate valueDate) {
		this.valueDate = valueDate;
	}
}
