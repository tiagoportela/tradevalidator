package com.tiagoportela.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tiagoportela.contraints.daos.CounterpartyDAO;

@Service
public class CounterpartyService {
	
	@Autowired
	private CounterpartyDAO counterpartyDAO;
	
	public boolean validateCounterparty(String counterparty) {
		return counterpartyDAO.findAll().contains(counterparty);
	}
}
