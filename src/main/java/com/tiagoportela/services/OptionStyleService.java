package com.tiagoportela.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tiagoportela.contraints.daos.OptionStyleDAO;

@Service
public class OptionStyleService {
	
	@Autowired
	private OptionStyleDAO optionStyleDAO;
	
	public boolean validateOptionStyle(String style) {
		final List<String> styles = optionStyleDAO.findAll();
		return styles.contains(style.toLowerCase()) || styles.contains(style.toUpperCase());
	}
}
