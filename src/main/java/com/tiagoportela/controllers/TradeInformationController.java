package com.tiagoportela.controllers;

import javax.validation.Valid;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tiagoportela.collections.TradeList;
import com.tiagoportela.models.GeneralTrade;

@RestController
public class TradeInformationController {
	
	private final Logger logger = LoggerFactory.getLogger(TradeInformationController.class);
	
	@PostMapping("/validate")
	public ResponseEntity<? extends GeneralTrade> validate(@RequestBody @Valid GeneralTrade trade, HttpServletRequest request) {
		logger.info("Incoming validation request from " + request.getRemoteAddr());
		logger.info("Trade data: " + trade);
		return new ResponseEntity<>(trade, HttpStatus.OK);
	}
	
	@PostMapping("/validateList")
	public ResponseEntity<TradeList> validateList(@RequestBody @Valid TradeList trades, HttpServletRequest request) {
		logger.info("Incoming validation request from " + request.getRemoteAddr());
		trades.getTrades().forEach(t -> logger.info("Trade data: " + t.toString()));
		return new ResponseEntity<>(trades, HttpStatus.OK);
	}
}
