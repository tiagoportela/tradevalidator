package com.tiagoportela.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.tiagoportela.validators.models.ValidationError;

@ControllerAdvice
public class RestErrorHandler {
	
	private final Logger logger = LoggerFactory.getLogger(RestErrorHandler.class);
	
    private static final String TRADE_INFORMATION_VALIDATION_ERROR = "Trade Information validation error";
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationError processValidationError(MethodArgumentNotValidException ex) {
    	ValidationError error = new ValidationError(HttpStatus.BAD_REQUEST.value(), TRADE_INFORMATION_VALIDATION_ERROR);
        BindingResult result = ex.getBindingResult();
        
        result.getGlobalErrors().forEach(e -> error.addFieldError(e.getCode(), e.getDefaultMessage()));
        result.getFieldErrors().forEach(e -> error.addFieldError(e.getField(), e.getDefaultMessage()));
        error.getFieldErrors().forEach(e -> logger.error(e.getField() + " - " + e.getMessage()));
        
        return error;
    }
}
