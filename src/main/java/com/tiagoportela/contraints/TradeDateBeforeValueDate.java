package com.tiagoportela.contraints;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.tiagoportela.validators.TradeDateBeforeValueDateValidator;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {TradeDateBeforeValueDateValidator.class})
public @interface TradeDateBeforeValueDate {
 
    String message() default "{com.tiagoportela.constraints.TradeDateBeforeValueDate.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
 
    String tradeDateField();
    String valueDateField();
}
