package com.tiagoportela.contraints;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.tiagoportela.validators.AmericanOptionExerciseDateValidationValidator;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {AmericanOptionExerciseDateValidationValidator.class})
public @interface AmericanOptionExerciseDateValidation {
 
    String message() default "{com.tiagoportela.constraints.AmericanOptionExerciseDateValidation.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
 
    String optionStyleField();
    String exerciseDateField();
    String tradeDateField();
    String expiryDateField();
}
