package com.tiagoportela.contraints;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.tiagoportela.validators.ExpiryAndPremiumDateBeforeDeliveryDateValidator;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ExpiryAndPremiumDateBeforeDeliveryDateValidator.class})
public @interface ExpiryAndPremiumDateBeforeDeliveryDate {
 
    String message() default "{com.tiagoportela.constraints.ExpiryAndPremiumDateBeforeDeliveryDate.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
 
    String expiryDateField();
    String premiumDateField();
    String deliveryDateField();
}
