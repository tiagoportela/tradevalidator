package com.tiagoportela.contraints;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.tiagoportela.validators.Iso4217CurrencyPairValidator;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {Iso4217CurrencyPairValidator.class})
public @interface Iso4217CurrencyPair {
 
    String message() default "{com.tiagoportela.constraints.Iso4217CurrencyPair.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
