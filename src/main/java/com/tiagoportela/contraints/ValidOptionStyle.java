package com.tiagoportela.contraints;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.tiagoportela.validators.ValidOptionStyleValidator;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ValidOptionStyleValidator.class})
public @interface ValidOptionStyle {
 
    String message() default "{com.tiagoportela.constraints.ValidOptionStyle.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
