package com.tiagoportela.contraints;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.tiagoportela.validators.Iso4217CurrencyValidator;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {Iso4217CurrencyValidator.class})
public @interface Iso4217Currency {
 
    String message() default "{com.tiagoportela.constraints.Iso4217Currency.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
