package com.tiagoportela.contraints.daos;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class OptionStyleDAO {

private List<String> databaseMock;
	
	public OptionStyleDAO() {
		this.databaseMock = Arrays.asList("AMERICAN", "EUROPEAN");
	}

	public List<String> findAll() {
		return databaseMock;
	}
}
