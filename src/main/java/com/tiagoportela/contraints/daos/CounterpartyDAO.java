package com.tiagoportela.contraints.daos;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class CounterpartyDAO {
	
	private List<String> databaseMock;
	
	public CounterpartyDAO() {
		this.databaseMock = Arrays.asList("JUPITER1", "JUPITER2");
	}

	public List<String> findAll() {
		return databaseMock;
	}
}
