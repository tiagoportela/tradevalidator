package com.tiagoportela.contraints;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.tiagoportela.validators.SupportedCounterpartyValidator;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {SupportedCounterpartyValidator.class})
public @interface SupportedCounterparty {
 
    String message() default "{com.tiagoportela.constraints.SupportedCounterparty.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
