package com.tiagoportela.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tiagoportela.contraints.ValidOptionStyle;
import com.tiagoportela.services.OptionStyleService;

public class ValidOptionStyleValidator implements ConstraintValidator<ValidOptionStyle, String> {

	private final Logger logger = LoggerFactory.getLogger(ValidOptionStyleValidator.class);
	
	@Autowired
	private OptionStyleService optionStyleService;
	
	@Override
	public void initialize(ValidOptionStyle constraint) {}

	@Override
	public boolean isValid(String style, ConstraintValidatorContext context) {
		try {
			return optionStyleService.validateOptionStyle(style);
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			return false;
		}
	}
}
