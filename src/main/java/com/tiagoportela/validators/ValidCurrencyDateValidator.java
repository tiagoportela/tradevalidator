package com.tiagoportela.validators;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tiagoportela.contraints.ValidCurrencyDate;

public class ValidCurrencyDateValidator implements ConstraintValidator<ValidCurrencyDate, LocalDate> {

	private final Logger logger = LoggerFactory.getLogger(ValidCurrencyDateValidator.class);

	private static final int SATURDAY = 6;
	private static final int SUNDAY = 7;
	private static List<Integer> weekendDaysList = Arrays.asList(SATURDAY, SUNDAY);

	@Override
	public void initialize(ValidCurrencyDate constraint) {}

	@Override
	public boolean isValid(LocalDate date, ConstraintValidatorContext context) {
		try {
			return !weekendDaysList.contains(date.getDayOfWeek().getValue());
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			return false;
		}
	}
}
