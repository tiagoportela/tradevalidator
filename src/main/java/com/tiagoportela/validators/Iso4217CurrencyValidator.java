package com.tiagoportela.validators;

import java.util.Currency;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tiagoportela.contraints.Iso4217Currency;

public class Iso4217CurrencyValidator implements ConstraintValidator<Iso4217Currency, String> {

	private final Logger logger = LoggerFactory.getLogger(Iso4217CurrencyValidator.class);
	
	@Override
	public void initialize(Iso4217Currency constraint) {}

	@Override
	public boolean isValid(String currency, ConstraintValidatorContext context) {
		try {
			this.validateCurrencyCode(currency);
			return true;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			return false;
		}
	}

	private void validateCurrencyCode(String currency) {
		Currency.getInstance(currency).getNumericCode();
	}
}
