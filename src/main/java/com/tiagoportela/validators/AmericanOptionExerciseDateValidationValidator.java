package com.tiagoportela.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tiagoportela.contraints.AmericanOptionExerciseDateValidation;

public class AmericanOptionExerciseDateValidationValidator extends DateValidator implements ConstraintValidator<AmericanOptionExerciseDateValidation, Object> {
	
	private static final String AMERICAN_STYLE_OPTION = "AMERICAN";

	private final Logger logger = LoggerFactory.getLogger(AmericanOptionExerciseDateValidationValidator.class);
	
    private String optionStyleField;
    private String exerciseDateField;
    private String tradeDateField;
    private String expiryDateField;
    
    @Override
    public void initialize(AmericanOptionExerciseDateValidation constraint) {
    	optionStyleField = constraint.optionStyleField();
    	exerciseDateField = constraint.exerciseDateField();
    	tradeDateField = constraint.tradeDateField();
    	expiryDateField = constraint.expiryDateField();
    }
 
    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
    	try {
            final Object optionStyle = this.getFieldValue(object, optionStyleField);
            final Object exerciseDate = this.getFieldValue(object, exerciseDateField);
            final Object tradeDate = this.getFieldValue(object, tradeDateField);
            final Object expiryDate = this.getFieldValue(object, expiryDateField);
            final boolean isAmericanOptionStyle = this.validateStyle(optionStyle);
            
            return !isAmericanOptionStyle || this.validateDates(exerciseDate, tradeDate, expiryDate);
        } catch (Exception exception) {
        	logger.error(exception.getMessage());
        	return false;
        }
    }

	private boolean validateStyle(Object optionStyle) {
		return optionStyle.toString().equalsIgnoreCase(AMERICAN_STYLE_OPTION);
	}

	private boolean validateDates(Object exerciseDate, Object tradeDate, Object expiryDate) {
		return !this.validateDate1BeforeDate2(exerciseDate, tradeDate) && this.validateDate1BeforeDate2(exerciseDate, expiryDate);
	}
}
