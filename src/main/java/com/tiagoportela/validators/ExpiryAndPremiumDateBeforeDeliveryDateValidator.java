package com.tiagoportela.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tiagoportela.contraints.ExpiryAndPremiumDateBeforeDeliveryDate;

public class ExpiryAndPremiumDateBeforeDeliveryDateValidator extends DateValidator implements ConstraintValidator<ExpiryAndPremiumDateBeforeDeliveryDate, Object> {
	
	private final Logger logger = LoggerFactory.getLogger(ExpiryAndPremiumDateBeforeDeliveryDateValidator.class);
	
    private String expiryDateField;
    private String premiumDateField;
    private String deliveryDateField;
 
    @Override
    public void initialize(ExpiryAndPremiumDateBeforeDeliveryDate constraint) {
    	expiryDateField = constraint.expiryDateField();
    	premiumDateField = constraint.premiumDateField();
    	deliveryDateField = constraint.deliveryDateField();
    }
 
    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
    	try {
            final Object expiryDate = this.getFieldValue(object, expiryDateField);
            final Object premiumDate = this.getFieldValue(object, premiumDateField);
            final Object deliveryDate = this.getFieldValue(object, deliveryDateField);
            
            return this.validateDate1BeforeDate2(expiryDate, deliveryDate) && this.validateDate1BeforeDate2(premiumDate, deliveryDate);
        } catch (Exception exception) {
        	logger.error(exception.getMessage());
        	return false;
        }
    }
}
