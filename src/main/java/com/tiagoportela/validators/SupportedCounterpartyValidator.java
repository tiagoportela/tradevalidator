package com.tiagoportela.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tiagoportela.contraints.SupportedCounterparty;
import com.tiagoportela.services.CounterpartyService;

public class SupportedCounterpartyValidator implements ConstraintValidator<SupportedCounterparty, String> {

	private final Logger logger = LoggerFactory.getLogger(SupportedCounterpartyValidator.class);
	
	@Autowired
	private CounterpartyService counterpartyService;
	
	@Override
	public void initialize(SupportedCounterparty constraint) {}

	@Override
	public boolean isValid(String counterparty, ConstraintValidatorContext context) {
		try {
			return counterpartyService.validateCounterparty(counterparty);
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			return false;
		}
	}
}
