package com.tiagoportela.validators;

import java.util.Arrays;
import java.util.Currency;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tiagoportela.contraints.Iso4217CurrencyPair;

public class Iso4217CurrencyPairValidator implements ConstraintValidator<Iso4217CurrencyPair, String> {

	private static final String CURRENCY_PAIR_SEPARATOR = "/";
	private static final String REGEX_3_CHARS = "(?<=\\G...)";
	private final Logger logger = LoggerFactory.getLogger(Iso4217CurrencyPairValidator.class);
	
	@Override
	public void initialize(Iso4217CurrencyPair constraint) {}

	@Override
	public boolean isValid(String currencyPair, ConstraintValidatorContext context) {
		try {
			final List<String> currencies = Arrays.asList(splitCurrencyPair(currencyPair));
			this.validateCurrenciesCode(currencies);
			return true;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			return false;
		}
	}

	private void validateCurrenciesCode(List<String> currencies) {
		for (String currency : currencies) {
			Currency.getInstance(currency).getNumericCode();
		}
	}

	private String[] splitCurrencyPair(String currencyPair) {
		final boolean hasSeparator = currencyPair.contains(CURRENCY_PAIR_SEPARATOR);
		final String[] currencies;
		
		if (hasSeparator) {
			currencies = currencyPair.split(CURRENCY_PAIR_SEPARATOR);
		} else {
			currencies = currencyPair.split(REGEX_3_CHARS);
		}
		
		return currencies;
	}
}
