package com.tiagoportela.validators.models;

import java.util.ArrayList;
import java.util.List;

public class ValidationError {
    private final int status;
    private final String message;
    private List<CustomFieldError> fieldErrors = new ArrayList<>();

    public ValidationError(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void addFieldError(String path, String message) {
    	CustomFieldError error = new CustomFieldError(path, message);
        fieldErrors.add(error);
    }

    public List<CustomFieldError> getFieldErrors() {
        return fieldErrors;
    }
}
