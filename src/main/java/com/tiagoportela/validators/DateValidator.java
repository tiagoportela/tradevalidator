package com.tiagoportela.validators;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public abstract class DateValidator {
	
	public boolean validateDate1BeforeDate2(final Object date1, final Object date2) {
    	return ((LocalDate)date1).isBefore((LocalDate)date2);
    }
	
	public Object getFieldValue(Object object, String fieldName) throws Exception {
        Class<?> clazz = object.getClass();
        Field field = getField(fieldName, clazz);

        if (field == null) {
        	field = getField(fieldName, clazz.getSuperclass());
        }
        
        field.setAccessible(true);
        return field.get(object);
    }

	private Field getField(String fieldName, Class<?> clazz) {
		for(Field f: Arrays.asList(clazz.getDeclaredFields())) {
        	if (f.getName().equals(fieldName)) {
        		return f;
        	}
        }
		return null;
	}
    
    public static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }

        return fields;
    }
}
