package com.tiagoportela.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tiagoportela.contraints.TradeDateBeforeValueDate;

public class TradeDateBeforeValueDateValidator extends DateValidator implements ConstraintValidator<TradeDateBeforeValueDate, Object> {
	
	private final Logger logger = LoggerFactory.getLogger(TradeDateBeforeValueDateValidator.class);
	
    private String tradeDateField;
    private String valueDateField;
 
    @Override
    public void initialize(TradeDateBeforeValueDate constraint) {
    	tradeDateField = constraint.tradeDateField();
    	valueDateField = constraint.valueDateField();
    }
 
    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
    	try {
            final Object tradeDate = this.getFieldValue(object, tradeDateField);
            final Object valueDate = this.getFieldValue(object, valueDateField);
            return this.validateDate1BeforeDate2(tradeDate, valueDate);
        } catch (Exception exception) {
        	logger.error(exception.getMessage());
        	return false;
        }
    }
}
