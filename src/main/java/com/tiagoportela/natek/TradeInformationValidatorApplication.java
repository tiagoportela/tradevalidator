package com.tiagoportela.natek;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.tiagoportela")
public class TradeInformationValidatorApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(TradeInformationValidatorApplication.class);
	
	public static void main(String[] args) {
		logger.info("Starting trade validation service");
		SpringApplication.run(TradeInformationValidatorApplication.class, args);
	}
}
