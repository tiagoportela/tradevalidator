package com.tiagoportela.collections;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.tiagoportela.models.GeneralTrade;

public class TradeList {

    @Valid
    private List<GeneralTrade> trades;

    public TradeList() {
        this.trades = new ArrayList<GeneralTrade>();
    }

    public TradeList(List<GeneralTrade> trades) {
        this.trades = trades;
    }

	public List<GeneralTrade> getTrades() {
		return trades;
	}

	public void setTrades(List<GeneralTrade> trades) {
		this.trades = trades;
	}
}